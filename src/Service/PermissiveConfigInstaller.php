<?php

namespace Drupal\install_config_override\Service;

use Drupal\Core\Config\ConfigInstallerInterface;
use Drupal\Core\Config\PreExistingConfigException;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Psr\Log\LoggerInterface;

/**
 * A configuration installer that can allow replacement of existing site config.
 *
 * This is meant to decorate Drupal core's normal config installer. That normal
 * installer will not allow config from one module to overwrite the config of
 * another. This one overrides that behavior to allow install profile
 * sub-modules to selectively override config that already exists on the site by
 * implementing hook_install_config_overrides().
 */
class PermissiveConfigInstaller implements ConfigInstallerInterface {

  /**
   * The logger to use for relaying messages.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The real installer that this instance is wrapping.
   *
   * @var \Drupal\Core\Config\ConfigInstallerInterface
   */
  protected $actualInstaller;

  /**
   * The Drupal module handler used for invoking hooks.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor for PermissiveConfigInstaller.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger to use for relaying messages from the new instance.
   * @param \Drupal\Core\Config\ConfigInstallerInterface $actual_installer
   *   The real installer that the new instance will wrap.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface|null $module_handler
   *   The Drupal module handler used for invoking hooks.
   */
  public function __construct(LoggerInterface $logger,
                              ConfigInstallerInterface $actual_installer,
                              ModuleHandlerInterface $module_handler) {
    $this->logger          = $logger;
    $this->actualInstaller = $actual_installer;
    $this->moduleHandler   = $module_handler;
  }

  /**
   * {@inheritDoc}
   *
   * This implementation suppresses PreExistingConfigException when installing
   * config for which a module has declared an override.
   *
   * @see hook_install_config_overrides()
   */
  public function checkConfigurationToInstall($type, $name) {
    try {
      $this->getActualInstaller()->checkConfigurationToInstall($type, $name);
    }
    catch (PreExistingConfigException $ex) {
      $configs = $ex->getConfigObjects();

      $flat_configs =
        PreExistingConfigException::flattenConfigObjects($configs);

      if ($this->doesModuleDeclareAllOverrides($name, $flat_configs)) {
        $this->getLogger()->notice(
          'Suppressing pre-existing config error for one or more configurations (@configs) to allow override by %module.',
          [
            '@configs'  => implode(', ', $flat_configs),
            '%module'   => $name,
          ]
        );
      }
      else {
        // Re-throw; this config should not be overridden.
        throw $ex;
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function installDefaultConfig($type, $name) {
    $this->getActualInstaller()->installDefaultConfig($type, $name);
  }

  /**
   * {@inheritDoc}
   */
  public function installOptionalConfig(StorageInterface $storage = NULL,
                                        $dependency = []) {
    $this->getActualInstaller()->installOptionalConfig($storage, $dependency);
  }

  /**
   * {@inheritDoc}
   */
  public function installCollectionDefaultConfig($collection) {
    $this->getActualInstaller()->installCollectionDefaultConfig($collection);
  }

  /**
   * {@inheritDoc}
   */
  public function setSourceStorage(StorageInterface $storage) {
    return $this->getActualInstaller()->setSourceStorage($storage);
  }

  /**
   * {@inheritDoc}
   */
  public function getSourceStorage(): ?StorageInterface {
    return $this->getActualInstaller()->getSourceStorage();
  }

  /**
   * {@inheritDoc}
   */
  public function setSyncing($status) {
    return $this->getActualInstaller()->setSyncing($status);
  }

  /**
   * {@inheritDoc}
   */
  public function isSyncing(): bool {
    return $this->getActualInstaller()->isSyncing();
  }

  /**
   * Gets the logger to use for relaying messages.
   *
   * @return \Psr\Log\LoggerInterface
   *   An instance of the logger for this module.
   */
  protected function getLogger(): LoggerInterface {
    return $this->logger;
  }

  /**
   * Gets the real installer that this instance is wrapping.
   *
   * Our class is merely a facade around the real instance.
   *
   * @return \Drupal\Core\Config\ConfigInstallerInterface
   *   An instance of Drupal's configuration installer service.
   */
  protected function getActualInstaller(): ConfigInstallerInterface {
    return $this->actualInstaller;
  }

  /**
   * Gets the Drupal module handler used for invoking hooks.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  public function getModuleHandler(): ModuleHandlerInterface {
    return $this->moduleHandler;
  }

  /**
   * Determines if the specified module has declared all of the given overrides.
   *
   * Overrides are declared via hook_install_config_overrides().
   *
   * @param string $module
   *   The machine name of the module.
   * @param string[] $overridden_config_ids
   *   The IDs of the configuration objects that are being overridden by the
   *   module.
   *
   * @return bool
   *   - TRUE if the specified module declares that it overrides all of the
   *     provided configs.
   *   - FALSE if either:
   *     1. Specified module does not declare any configuration overrides; OR
   *     2. At least one of the configuration IDs does not appear in the list of
   *        overrides that the module declares.
   */
  protected function doesModuleDeclareAllOverrides(
                                          string $module,
                                          array $overridden_config_ids): bool {
    $declared_config_override_ids = $this->getConfigOverridesForModule($module);

    $undeclared_overrides =
      array_diff($overridden_config_ids, $declared_config_override_ids);

    return empty($undeclared_overrides);
  }

  /**
   * Gets all configurations that the specified module declares it overrides.
   *
   * @param string $module
   *   The machine name of the module for which overrides are needed.
   */
  protected function getConfigOverridesForModule(string $module): array {
    // Load the .install file to get hook_install_config_overrides.
    module_load_install($module);

    $module_handler = $this->getModuleHandler();

    $config_overrides =
      $module_handler->invoke($module, 'install_config_overrides');

    if (empty($config_overrides) || !is_array($config_overrides)) {
      return [];
    }
    else {
      return $config_overrides;
    }
  }

}

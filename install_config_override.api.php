<?php

/**
 * @file
 * Hooks related to the Install-time Config Override module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Declares the ID of each configuration that this module overrides.
 *
 * This hook should be placed in the ".install" file of the module that is
 * providing the configuration overrides.
 *
 * Declaring a configuration in this list does not automatically install the
 * configuration file. Each config file needs to be placed in the
 * "config/install" folder in the module that declares this hook. Configs are
 * only installed when the module is being installed.
 *
 * If multiple modules override the same config, the last one in wins. You may
 * need to use hook_module_implements_alter() to control the order in which
 * such overrides are applied.
 *
 * If the module includes a config override that does not appear in the list of
 * IDs returned by this hook, it is handled the same as if the module did not
 * declare this hook -- a Drupal\Core\Config\PreExistingConfigException will be
 * thrown by the module installer and the module will not be installed.
 *
 * @return string[]
 *   An array containing the machine name of each config that this module is
 *   expected to override.
 */
function hook_install_config_overrides(): array {
  // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
  /** @noinspection PhpUnnecessaryLocalVariableInspection */
  $overrides = [
    'automated_cron.settings',
    'update.settings',
  ];

  return $overrides;
}

/**
 * @} End of "addtogroup hooks".
 */

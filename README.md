# Install-time Config Override

## Purpose
As you may know, Drupal Core allows installation profiles to provide 
configurations that override default config provided by core and other modules.
Unfortunately, other modules -- even sub-modules of the installation profile -- 
are not afforded the same capability. As a result, it is difficult for
installation profile maintainers to accomplish in Drupal 8/9 what was possible
with the Features module in Drupal 7 -- the ability to break up config into 
logical sub-modules.

Another place where Core's default behavior is problematic is optional 
sub-modules -- an installation profile can only override config one time, at
profile install time. If you want to provide functionality in your installation 
profile that may not be needed on all deployments (e.g. a feature that an admin
can opt out of enabling during site install), you normally don't have a way to
override the config that such a feature requires. In order to accomplish this,
you'd need a way to put the config for that feature into the feature itself but
Drupal Core won't allow two modules to provide the same configs. If you
pre-install the config at profile level, then you will run into errors when the
module that actually needs that config gets installed.

This module fixes both of the limitations mentioned above. It allows modules to
provide configurations that override the default configurations provided by
other modules. It accomplishes this by allowing such a module to implement a
hook to declare which config(s) it is providing that should be allowed to
override configuration that already exists on the site.

It's important to stress that this module is intended only for use by
installation profiles. It should most certainly not be used as a dependency of
other contrib. modules, since that introduces the messy potential for multiple
contrib. modules to override each other's configs (e.g. what if 3 contrib
modules overrode comment.settings.yml -- which would be expected to win?).

## How to Use
1. Install the module through Composer.
2. Add the module as a dependency of the installation profile.
3. Within each sub-module that provides config to override default config of
   other modules, implement `hook_install_config_overrides()` so that it returns
   the machine name of each config that is being overridden.
4. Install profile and sub-modules as normal.
